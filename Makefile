#
# Use this Makefile with GNU make.
#
TOP ?= example-presentation.pdf
TOP_NAME = $(patsubst %.pdf,%,$(TOP))
EXT = pdf log aux out bbl blg toc snm nav fdb_latexmk fls

STYS       = $(wildcard texinputs/*.sty)

FIGS       = $(wildcard figs/*.fig)
FIGS_PDF   = $(patsubst %.fig,%.pdftex,$(FIGS))
FIGS_PDF_T = $(patsubst %.fig,%.pdftex_t,$(FIGS))

SVGS       = $(wildcard svgs/*.svg)
SVGS_PDF   = $(patsubst %.svg,%.pdf,$(SVGS))

ODGS       = $(wildcard odgs/*.odg)
ODGS_PDF   = $(patsubst %.odg,%.pdf,$(ODGS))

NEEDED = $(FIGS_PDF) $(FIGS_PDF_T) $(SVGS_PDF) $(ODGS_PDF)

# Only for package documentation.
NEEDED_DOC = $(TOP_NAME)-slide2.pdf

PREVIEW_OPTS  = \RequirePackage[active,delayed,tightpage,graphics,pdftex] {preview}
PREVIEW_OPTS += \PreviewMacro[{*[][]{}}]{\incode}

export TEXINPUTS := ./texinputs/:$(TEXINPUTS)

#.SECONDARY: $(FIGS_PDF)

.PHONY: all clean dep preview

all: $(TOP)

$(TOP) : dep

dep: $(NEEDED) $(NEEDED_DOC)

%.pdf:%.tex $(STYS)
	@echo "LaTeX search path $(TEXINPUTS)"
	@latexmk -pdf $<

preview: dep
	pdflatex  '$(PREVIEW_OPTS) \input{$(TOP_NAME).tex}'

clean:
	@echo Cleaning $(TOP) and $(NEEDED)
	@$(foreach file,$(TOP_NAME) $(TOP_NAME)-slide2 preview texput,$(foreach ext,$(EXT),[ -e $(file).$(ext) ] && rm -f $(file).$(ext) || true;))
	@rm -f $(NEEDED)
	@rm -f $(patsubst %.pdf, %.tex, $(NEEDED_DOC))


%.pdftex:%.fig
	fig2dev -L pdftex $< $@
%.pdftex_t:%.pdftex
	fig2dev -L pdftex_t -p $< $(patsubst %.pdftex_t,%.fig,$@) $@

%.swf:%.pdf
	pdf2swf $< && chmod -x $@

%.pdf:%.svg
	inkscape --export-type=pdf -o $@ $<

%.pdf:%.odg
	libreoffice --headless --convert-to pdf $< --outdir odgs
	pdfcrop --margins 1 $@ $@

# Only for package documentation.
$(TOP_NAME)-slide2.tex : $(TOP_NAME).tex
	@perl -pE 'if (m{^%+ Stop ici}i) { say "\\end{document}"; last; }' $< > $@
