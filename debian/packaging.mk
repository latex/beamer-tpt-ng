#! /usr/bin/make -f
#
# Use this Makefile with GNU make.
#

# Package name, and release information through include file.
# FIXME handle relative directories more robustly.
package         = theme-tpt
include version.mk

# Default installation directories.
prefix          = /usr/local
texmfdir        = $(prefix)/share/texmf
texdir          = $(texmfdir)/tex/latex/$(package)
picdir          = $(texdir)
docdir          = $(texmfdir)/doc/latex/$(package)

TOP             ?= example-presentation.pdf
TOP_NAME        = $(patsubst %.pdf,%,$(TOP))

# FIXME temporary: ignore poster .sty in $(STYS) until it is updated.
STYS_TMP        = $(wildcard texinputs/*.sty)
STYS            = $(filter-out %poster.sty, $(STYS_TMP))
LOGOS           = $(wildcard texinputs/logo-*)
DOCS_PDF        = $(TOP)# FIXME have list of compiled PDFs.
DOCS_TEX        = $(patsubst %.pdf,%.tex,$(DOCS_PDF))
DOCS            = $(DOCS_PDF) $(DOCS_TEX)

# Program names.
RELEASE_SCRIPT  = debian/update-release
MKTEXLSR        = mktexlsr
INSTALL         = install
PERL            = perl
ZIP             = zip

.PHONY: install install-tex install-doc install-nomktexlsr
.PHONY: build-doc release deb zip

# Only for package installation and packaging.

# Build documentation (example files): use Makefile above.
# FIXME fix Makefile above, fix relative directories.
build-doc:
	$(MAKE) all

# Install.  Separate installation of TeX files and compiled docs.
install: install-nomktexlsr
	$(MKTEXLSR) $(texmfdir)

install-nomktexlsr: install-tex install-doc

install-tex: $(STYS) $(LOGOS)
	$(INSTALL) -d $(texdir)
	$(INSTALL) -m 644 $(STYS) $(texdir)
	$(INSTALL) -d $(picdir)
	$(INSTALL) -m 644 $(LOGOS) $(picdir)

install-doc: build-doc
	$(INSTALL) -d $(docdir)
	$(INSTALL) -m 644 $(DOCS) $(docdir)

# Create new release.
# Usage: make release V=<x.y.z> D=<yyyy-mm-dd>.  If V and D are not given,
# they will be calculated from the latest version-like tag
# (and incremented unless working copy is clean and at the tag).
V =
D =
NEW_RELEASE_V = $(patsubst %,-v %,$(V))
NEW_RELEASE_D = $(patsubst %,-d %,$(D))
release:
	$(PERL) -i.orig $(RELEASE_SCRIPT) $(NEW_RELEASE_V) $(NEW_RELEASE_D) \
		$(STYS) $(TOP_NAME).tex version.mk
# FIXME also handle debian/changelog.

# Create zip archive.
zip:
	$(ZIP) $(package)-$(RELEASE_VERSION).zip $(STYS) $(LOGOS) $(DOCS)

# Create Debian package.
# FIXME auto-generate debian/latex-theme-tpt-doc-fr.links
deb:
	dpkg-buildpackage -us -uc -b
